import { getUsername } from "../../GetUsername";
import React, { Component } from "react";
import axios from "../../APIs";
import moment from "moment";

import {
  Header,
  Step,
  Card,
  Dropdown,
  Button,
  Segment,
  Image,
  Icon,
  Popup,
  Modal,
  Grid,
  Feed,
} from "semantic-ui-react";
import "./AlegereDataIntoarcere.css";
import { descarcaContractCazare } from "../../descarcaContractCazare";
import docsSources from "../../docsSources.json";

/**
 *
 * @type {string}
 * calea pentru fisierul cu actele necesare pentru cazare
 */
const pathActeNecesareCazare = docsSources.filePathActeCazare;

class AlegereDataIntoarcere extends Component {
  constructor() {
    super();
    this.state = {
      Username: [],
      anUniversitarCazare: [],
      idAnUniversitarCazare: [],
      idStudent: [],
      idCamera: [],
      numarCamera: [],
      caminCazare: [],
      numeStudent: [],
      denumireFacultate: [],
      cazareExpirata: null,
      idCazare: [],
      intervalIntoarcere: [], //intervalul de decazare luat din api
      intervalIntoarcereFiltrare: [], //copie a intervalului de decazare folosit la filtrare
      loadedIntervalIntoarcere: false, //daca api-ul de Get_DateDisponibile s-a efectuat sau nu
      areCazare: true, //pentru loader, sa il afiseze daca studentul are cazare sau nu
      mesaj: "Nu ai nicio dată selectată", //mesaj pentru Segment
      dataRetinutaInBD: null, //data retinuta in baza de date ca data aleasa de student
      clicked: false, //daca butonul de confirmare a fost apasat sau nu
      checkboxCheck: false, //daca butonul "Datele sunt corecte" a fost apasat sau nu
      dataAleasaStep: false, //daca data a fost aleasa sau nu din dropdown
      modalOpen: false,
      redBtnClicked: false,
      mesajDropdown: "Alege data dorită",
      modalOpenConfirmare: false, //pentru modalul de confirmare ca s-a retinut in BD data aleasa

      dataAleasa: null,
      dataAcum: null,
    };
  }

  componentDidMount() {
    /**
     * este retinut username-ul studentului logat
     */
    const username = getUsername();
    this.setState({ Username: username });

    /**
     * se preia anul universitar curent
     */
    axios.get("AnUniversitar/AnUniversitarCazare").then((response) => {
      //console.log(response);
      this.setState({
        anUniversitarCazare: response.data,
        idAnUniversitarCazare: response.data.ID_AnUniv,
      });

      /**
       * sunt preluate datele de cazare in functie de username-ul studentului
       */
      axios
        .get(
          "Cazare/CazareGetByUsernameAnUniv?Username=" +
            username +
            "&ID_AnUniv=" +
            response.data.ID_AnUniv
        )
        .then((cazare) => {
          //console.log(cazare.data);

          this.setState({
            dataAcum: moment(),
          });

          if (cazare.data.length === 0) {
            this.setState({
              areCazare: false,
            });
          } else {
            this.setState({
              idCamera: cazare.data[0].ID_Camera,
              numarCamera: cazare.data[0].NumarCamera,
              caminCazare: cazare.data[0].DenumireCamin,
              cazareExpirata: cazare.data[0].CazareExpirataLaDataCurenta,
              idCazare: cazare.data[0].ID_Cazare,
              idStudent: cazare.data[0].ID_Student,
              numeStudent: cazare.data[0].Nume + " " + cazare.data[0].Prenume,
            });

            // console.log(this.state.numeStudent);
            // console.log(this.state.caminCazare);
            // console.log(this.state.numarCamera);
            // console.log(this.state.cazareExpirata);

            /**
             * preia intervalul de date alocat camerei studentului si le retine in formatul DD-MM-YYYY
             */
            axios
              .get(
                "DateIntoarcereCamin/Get_DateDisponibile?ID_Camera=" +
                  cazare.data[0].ID_Camera
              )
              .then((perioada) => {
                //console.log(perioada.data)
                let temp = [];
                perioada.data.forEach((d, index) => {
                  let data = {
                    key: index,
                    text: d,
                    value: d,
                  };
                  temp.push(data);
                });

                //console.log(temp);

                this.setState({
                  intervalIntoarcere: temp,
                  intervalIntoarcereFiltrare: temp,
                  loadedIntervalIntoarcere: true,
                });
                //console.log(this.state.intervalIntoarcere);

                /**
                 * preia data aleasa de student, retinuta in baza de date
                 */
                axios
                  .get(
                    "DateIntoarcereCamin/AfisareDataIntoarcereStudent?ID_Cazare=" +
                      cazare.data[0].ID_Cazare
                  )
                  .then((cazare) => {
                    //console.log(cazare.data);
                    this.setState({
                      dataRetinutaInBD: cazare.data
                        ? moment(cazare.data).format("DD.MM.YYYY")
                        : null,
                      mesaj:
                        "Data aleasă este " +
                        moment(cazare.data).format("DD.MM.YYYY"),
                    });
                  })
                  .catch((error) => {
                    console.log(error.response);
                  });
              })
              .catch((error) => {
                console.log(error.response);
              });
          }
        });
    });
  }

  deschideActeNecesareCazare = () => {
    const url = pathActeNecesareCazare;
    window.open(url, "_black");
  };

  /**
   *
   * @param mesaj
   * functia de afisare a mesajului pentru pagina
   */
  afisareMesaj = (mesaj) => {
    return (
      <Grid
        style={{
          paddingTop: "15vh",
        }}
      >
        <Grid.Row className="centrare">
          <Segment.Group>
            <Segment>
              <Feed size="large">
                <Feed.Event>
                  <Feed.Content>
                    <Feed.Summary>
                      <Feed.User>
                        Universitatea Transilvania din Brașov
                      </Feed.User>
                      <Feed.Date>{this.state.dataAcum?.format("LT")}</Feed.Date>
                    </Feed.Summary>
                    <Feed.Extra text>{mesaj}</Feed.Extra>
                  </Feed.Content>
                </Feed.Event>
              </Feed>
            </Segment>
          </Segment.Group>
        </Grid.Row>
      </Grid>
    );
  };

  // /**
  //  * functia de descarcare a documentelor pe care studentul le descarca
  //  */
  // descarcaDovadaDecazare = () => {
  //
  //     const linkSource = `${process.env.REACT_APP_URL_AGSISPI}Rapoarte/DovadaDecazare?username=${this.state.Username}&ID_AnUniv=${this.state.idAnUniversitarCazare}`;
  //     const downloadLink = document.createElement("a");
  //     const fileName = `Dovada decazare ${this.state.Username}.pdf`;
  //
  //     downloadLink.href = linkSource;
  //     downloadLink.download = fileName;
  //     document.body.appendChild(downloadLink);
  //     downloadLink.click();
  // };

  /**
   *
   * @param event
   * @param data data aleasa de student din dropdown
   *
   * metoda se apeleaza cand este selectata o data din dropdown si
   * retine data aleasa de student si o formateaza de tipul YYYY-MM-DD
   */
  retineDataAleasa = (event, data) => {
    this.setState({
      dataAleasa: moment(data.value, "DD.MM.YYYY").format("YYYY-MM-DD"),
      dataAleasaStep: true,
      mesajDropdown: data.value,
    });
    //console.log(this.state.dataAleasa);
  };

  /**
   * metoda se apeleaza cand este apasat butonul "Confirma data aleasa"
   */
  confirmareData = () => {
    //console.log(this.state.dataAleasa);

    this.setState({
      clicked: true,
    });

    /**
     * daca butonul a fost apasat, se retine prin post data aleasa
     * in cazul in care apare o eroare, studentul este anuntat sa aleaga o alta data
     */
    if (!this.state.clicked) {
      axios
        .post(
          "DateIntoarcereCamin/AdaugareDataIntoarcereCamin?ID_Camera=" +
            this.state.idCamera +
            "&ID_Cazare=" +
            this.state.idCazare +
            "&data=" +
            this.state.dataAleasa,
          {
            DataIntoarcereCamin: this.state.dataAleasa,
          }
        )
        .then((perioada) => {
          let temp = [];
          perioada.data.forEach((d, index) => {
            let data = {
              key: index,
              text: d,
              value: d,
            };
            temp.push(data);
          });

          //console.log(temp);
          this.setState({
            intervalIntoarcere: temp,
            modalOpenConfirmare: true,
            dataRetinutaInBD: this.state.dataAleasa,
          });
        })
        .catch((error) => {
          console.error(error);
          if (error.response) {
            if (error.response.status === 409) {
              console.error("ErrorCode", error.response.status);
              alert(
                "Alege o altă dată pentru cazare, un alt coleg de cameră a fost mai rapid decat tine!"
              );
            } else {
              alert("A aparut o eroare la salvarea datelor");
            }
          }
        })
        .finally(() => {
          this.setState({ clicked: false });
        });
      this.setState({
        clicked: true,
      });
    }

    this.setState({
      mesaj:
        "Data aleasă este " +
        moment(this.state.dataAleasa).format("DD.MM.YYYY"),
      checkboxCheck: false,
      dataAleasaStep: false,
      clicked: false,
      mesajDropdown: "Alege data dorită",
    });
  };

  /**
   * metoda apelata atunci cand este apasat butonul de 'Datele sunt corecte'
   */
  toggle = () => {
    if (this.state.checkboxCheck)
      this.setState({
        checkboxCheck: false,
        mesajDropdown: "Alege data dorită",
        dataAlesaStep: false,
      });
    else
      this.setState({
        checkboxCheck: true,
        mesajDropdown: "Alege data dorită",
        dataAlesaStep: false,
      });
  };

  /**
   * metoda apelata cand Modalul de infirmare este inchis
   */
  handleModalClose = () => {
    this.setState({
      modalOpen: false,
    });
  };

  /**
   * metoda apelata cand Modalul este deschis
   */
  handleModalOpen = () => {
    this.setState({
      modalOpen: true,
      checkboxCheck: false,
      clicked: false,
      mesajDropdown: "Alege data dorită",
      dataAleasa: null,
    });
  };

  /**
   * metoda apelata cand Modalul de confirmare este inchis
   */
  handleModalConfirmareClose = () => {
    this.setState({
      modalOpenConfirmare: false,
    });
  };

  /**
   * metoda apelata cand este apasat butonul rosu
   * campurile sunt resetate
   */
  handleRedBtn = () => {
    this.setState({
      clicked: false,
      checkboxCheck: false,
      dataAleasaStep: false,
      mesajDropdown: "Alege data dorită",
    });
  };

  /**
   *
   * @returns {boolean}
   * daca fereasta este mai mica de 700px sa nu mai apara Popup-urile
   */
  popupDisabler = () => {
    return window.innerWidth < 700;
  };

  /**
   * functia care permite descarcarea contractului de cazare
   */
  handleDecazeaza = () => {
    descarcaContractCazare(this.state.idCazare, this.state.numeStudent);
  };

  render() {
    /**
     * daca studentul are cazare si datele de decazare sunt nenule, se afiseaza pagina
     * in caz contrar, daca studentul nu are cazare, se afiseaza loaderul de 'Nu ai nicio cazare activa'
     * in caz contrar, daca studentul are datele de cazare nule, se afiseaza loaderul de 'Vă rugăm să reveniți, datele pentru decazare sunt încă indisponibile!'
     */

    return (
      <Grid centered stackable>
        <Grid.Row>
          <div style={{ width: "50%", paddingTop: "5vh" }}>
            <Header as="h1" className="header">
              Pagina de programare a cazării
            </Header>

            <Header as="h2" className="paddingCardBottom">
              {this.state.anUniversitarCazare.Denumire}
            </Header>
          </div>
        </Grid.Row>

        {this.state.areCazare ? (
          this.state.loadedIntervalIntoarcere ? (
            <div>
              <Grid
                stackable
                style={{
                  paddingLeft: "5vh",
                  paddingRight: "5vh",
                  paddingTop: "5vh",
                }}
                centered
              >
                <Grid.Row
                  style={{
                    paddingBottom: "3vh",
                  }}
                  align="center"
                >
                  <Step.Group ordered widths={3}>
                    <Step
                      completed={this.state.checkboxCheck}
                      className="steps"
                    >
                      <Step.Content className="steps">
                        <Step.Title>Verifică</Step.Title>
                        <Step.Description>
                          Verifică dacă datele de cazare (cămin, cameră, nume)
                          sunt corecte
                        </Step.Description>
                      </Step.Content>
                    </Step>

                    <Step completed={this.state.dataAleasaStep}>
                      <Step.Content className="steps">
                        <Step.Title>Alege data</Step.Title>
                        <Step.Description>
                          Alege dintre datele disponibile data când vei veni să
                          te cazezi
                        </Step.Description>
                      </Step.Content>
                    </Step>

                    <Step active>
                      <Step.Content className="steps">
                        <Step.Title>Confirmă</Step.Title>
                        <Step.Description>
                          Confirmă data aleasă prin apăsarea butonului
                        </Step.Description>
                      </Step.Content>
                    </Step>
                  </Step.Group>
                </Grid.Row>

                <Grid.Row
                  centered
                  verticalAlign="middle"
                  columns="equal"
                  style={{
                    paddingLeft: "2vw",
                    paddingRight: "2vw",
                  }}
                >
                  <Grid.Column>
                    <Card fluid>
                      <Card.Content textAlign="center">
                        <Card.Header>{this.state.numeStudent}</Card.Header>
                        <Card.Meta>
                          Cazat/ă în {this.state.caminCazare} Camera{" "}
                          {this.state.numarCamera}
                        </Card.Meta>
                        <Card.Description>
                          <Modal
                            size="small"
                            trigger={
                              <Popup
                                disabled={this.popupDisabler()}
                                position="bottom right"
                                size="small"
                                content="Apăsarea butonului va infirma corectitudinea datelor (căminul și/sau camera nu sunt corecte)"
                                trigger={
                                  <Button
                                    type={"button"}
                                    basic
                                    color="red"
                                    onClick={this.handleModalOpen}
                                    icon="close"
                                  />
                                }
                              />
                            }
                            open={this.state.modalOpen}
                            onClose={this.handleModalClose}
                          >
                            <Modal.Content>
                              <h4>
                                În cazul în care datele de cazare nu sunt
                                corecte, ia legătura cu administrația căminului
                                în care ești cazat/ă!
                              </h4>
                            </Modal.Content>
                            <Modal.Actions>
                              <Button
                                type={"button"}
                                basic
                                color="green"
                                onClick={this.handleModalClose}
                              >
                                <Icon name="checkmark" /> Ok
                              </Button>
                            </Modal.Actions>
                          </Modal>

                          <Popup
                            disabled={this.popupDisabler()}
                            position="bottom left"
                            size="small"
                            content="Apăsarea butonului va confirma corectitudinea datelor (căminul și camera sunt corecte)"
                            trigger={
                              <Button
                                type={"button"}
                                color="green"
                                onClick={this.toggle}
                                className="paddingCardBottom"
                                icon="check"
                              />
                            }
                          />
                        </Card.Description>
                      </Card.Content>
                    </Card>
                  </Grid.Column>

                  <Grid.Column>
                    <Dropdown
                      type={"button"}
                      text={this.state.mesajDropdown}
                      header="Alege data la care dorești să vii să te cazezi"
                      className="dropdown"
                      onChange={this.retineDataAleasa}
                      value={this.state.mesajDropdown}
                      button
                      floating
                      fluid
                      options={this.state.intervalIntoarcere}
                      disabled={!this.state.checkboxCheck}
                    />
                  </Grid.Column>

                  <Grid.Column verticalAlign="middle">
                    <Popup
                      size="small"
                      position="bottom center"
                      content="După confirmare, data aleasă poate fi schimbată în măsura datelor rămase nealocate!"
                      disabled={this.popupDisabler()}
                      trigger={
                        <Button
                          type={"button"}
                          size="big"
                          onClick={this.confirmareData}
                          disabled={
                            !this.state.dataAleasa || !this.state.checkboxCheck
                          }
                          className="btn"
                          color={
                            this.state.dataAleasaStep &&
                            this.state.checkboxCheck
                              ? "green"
                              : null
                          }
                        >
                          Confirmarea datei alese
                        </Button>
                      }
                    />
                    {/*<Popup
                    disabled={this.popupDisabler()}
                    size="small"
                    content="Poți deselecta pașii deja bifați"
                    trigger={
                      <Button
                        type={"button"}
                        size="small"
                        onClick={this.handleRedBtn}
                        color={
                          this.state.dataAleasaStep || this.state.checkboxCheck
                            ? "red"
                            : null
                        }
                        inverted={
                          this.state.dataAleasaStep || this.state.checkboxCheck
                        }
                      >
                        <div>
                          <Icon name="close" />
                        </div>
                      </Button>
                    }
                />*/}
                  </Grid.Column>
                </Grid.Row>
              </Grid>

              <Grid.Row
                centered
                style={{
                  paddingLeft: "1vw",
                }}
              >
                <Grid.Column>
                  {this.state.dataRetinutaInBD !== null && (
                    <div
                      className="btnDescarca"
                      align="center"
                      style={{
                        paddingLeft: "40vw",
                        paddingRight: "40vw",
                        paddingBottom: "27vh",
                      }}
                    >
                      <Segment
                        color={this.state.clicked ? "green" : "black"}
                        size="big"
                      >
                        {this.state.mesaj}
                      </Segment>
                    </div>
                  )}

                  {/*<div className='paddingCardTop'>*/}
                  {/*    <Button*/}
                  {/*        onClick={this.handleDecazeaza}*/}
                  {/*        type={"button"}*/}
                  {/*        size='large'*/}
                  {/*        className='btn'*/}
                  {/*        disabled={!this.state.dataRetinutaInBD}*/}
                  {/*        color={this.state.dataRetinutaInBD !== null ? 'green' : null}>*/}
                  {/*        <Icon name='download'/>*/}
                  {/*        Descarcă contract cazare*/}
                  {/*    </Button>*/}
                  {/*</div>*/}
                  <div className="paddingCardTop">
                    <Button
                      size="small"
                      type={"button"}
                      basic
                      color="green"
                      onClick={this.deschideActeNecesareCazare}
                    >
                      <Icon name="file alternate" />
                      Vezi actele necesare cazării!
                    </Button>
                  </div>
                </Grid.Column>
              </Grid.Row>

              <Modal
                size="small"
                open={this.state.modalOpenConfirmare}
                onClose={this.handleModalConfirmareClose}
              >
                <Modal.Content>
                  <h3>
                    Data de {moment(this.state.dataAleasa).format("DD.MM.YYYY")}{" "}
                    a fost înregistrată!
                  </h3>
                  <div>
                    Având în vedere contextul epidemiologic actual, pe baza
                    acestei planificări, pentru acest an universitar procesul de
                    cazare se va realiza astfel:
                  </div>
                  <ul>
                    <li>
                      Programul este între orele{" "}
                      <strong>
                        8:00 - 17:00 (8:00 - 14:00 în 4 octombrie)
                      </strong>
                    </li>
                    <li>
                      Accesul în cămin se face pe baza{" "}
                      <strong>programării online</strong>
                    </li>
                    <li>
                      La intrarea în cămin,{" "}
                      <strong>
                        fiecărui student i se va măsura temperatura{" "}
                      </strong>{" "}
                      (dacă temperatura este mai mare de 37 grade C, acesta nu
                      are acces în cămin)
                    </li>
                    <li>
                      Studentul trebuie să poarte{" "}
                      <strong>obligatoriu mască</strong> și
                      <strong> să își dezinfecteze mâinile</strong> utilizând
                      dispozitivul de la intrarea în cămin
                    </li>
                    <li>
                      <strong>
                        Persoanele care însoțesc studenții nu vor avea acces în
                        cămine
                      </strong>
                    </li>
                    <li>
                      <strong>Actele necesare pentru cazare</strong> se găsesc
                      pe site-ul <i>unitbv.ro</i>, la secțiunea Studenți >
                      Cazare și masă > Cămine și cazări >{" "}
                      <u>Acte necesare pentru cazare</u>
                    </li>
                    <li>
                      <strong>Contractul de cazare</strong> va putea fi
                      descărcat de la secțiunea <u>Contract Cazare</u>, din
                      platforma Intranet și este recomandat ca studenții să vină
                      cu documentele necesare <strong>printate</strong>
                      pentru o fluiditate cât mai bună a procesului
                    </li>
                    <li>
                      <strong>
                        Doar studenții internaționali (bursieri ai statului
                        român) vor avea locurile rezervate până vor putea ajunge
                        la cămin, în cazul în care nu vor putea confirma locul
                        în perioada stabilită
                      </strong>
                    </li>
                  </ul>
                </Modal.Content>
                <Modal.Actions>
                  <Button
                    type={"button"}
                    basic
                    color="green"
                    onClick={this.handleModalConfirmareClose}
                  >
                    <Icon name="checkmark" /> Da
                  </Button>
                </Modal.Actions>
              </Modal>
            </div>
          ) : (
            this.afisareMesaj(
              "Vă rugăm să reveniți, datele pentru programarea cazării sunt încă indisponibile!"
            )
          )
        ) : (
          this.afisareMesaj(
            "Nu aveți nicio cazare activă! Dacă ești anul I, Master, nu vei aparea cu cazare activă, iar cazarea în cămine se face conform programării realizate de Facultate."
          )
        )}
      </Grid>
    );
  }
}

export default AlegereDataIntoarcere;

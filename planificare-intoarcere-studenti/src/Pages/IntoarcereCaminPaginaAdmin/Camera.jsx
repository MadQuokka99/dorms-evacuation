import React, {Component} from 'react'
import {Segment, Button} from "semantic-ui-react";
import "./AlocareTermenCamera.css"
import moment from "moment";

class Camera extends Component {
  constructor(props) {
    super(props)
    this.state = {
      checked: false
    }
  }


  render() {
    return (
      <div key={this.props.ID_Camera}>
        <Segment
          key={this.props.ID_Camera}
          color={(this.props.dataInceput && this.props.dataSfarsit) ? 'green' : 'red'}
        >
          <input type="checkbox" name={this.props.NumarCamera} checked={this.props.checked}
                 value={this.props.ID_Camera} onChange={this.props.handleCheckbox}/>

          <span>
            <strong>{`Camera ${this.props.name} | `}</strong>
          </span>
          {
            (this.props.dataInceput || this.props.dataSfarsit) &&
            <span>{`${moment(this.props.dataInceput).format("DD.MM.YYYY")} - ${moment(this.props.dataSfarsit).format("DD.MM.YYYY")}`}</span>
          }


        </Segment>
      </div>
    )
  }
}

export default Camera;
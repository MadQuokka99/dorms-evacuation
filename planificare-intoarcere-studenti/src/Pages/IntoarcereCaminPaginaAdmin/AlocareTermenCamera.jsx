import React, {Component} from 'react'
import axios from "../../APIs";
import localAxios from "../../localAPIs";
import {getUsername} from "../../GetUsername";
import "./AlocareTermenCamera.css"
import Camera from "./Camera";
import moment from 'moment'
import 'moment/locale/ro';
import {Dropdown,  Grid, Segment, Button,  Menu} from "semantic-ui-react";
import {DateRangePicker} from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import 'react-dates/initialize';

class AlocareTermenCamera extends Component {

  state = {
    detaliiCamere: [],
    listaCamine: [],
    caminSelectat: null,
    idCaminSelectat: null,
    listaEtaje: [],
    etajSelectat: [],
    anUniversitarCurent: null,
    anCurentSiPrecedent: [],
    username: null,
    idfacultateCameraNew: null,
    ok: true,
    open: false,
    etajCurent: null,
    etajCamera: null,
    idCamera: '',
    nrLocuri: '',
    checked: true,
    verificat: false,
    idCameraChecked: [],
    dateCamera: []
  }

  handleOpen = () => this.setState({open: true})
  handleClose = () => this.setState({open: false})

  componentDidMount() {
    moment.locale('ro');
    const username = getUsername();
    this.setState({username: username});
    console.log(username)
    axios
      .get("api/AnUniversitar/AnUniversitarCurent")
      .then(anUniv => {
        console.log(anUniv)
        this.setState({
          anSelectat: anUniv.data.ID_AnUniv
        })
        axios
          .get(
            "api/Camin/CaminListByAnUniv?ID_AnUniv=" +
            anUniv.data.ID_AnUniv
          )
          .then(camine => {
            axios
              .get(
                "api/Camin/CaminListByUsernameAdministrator?UsernameAdministrator=" +
                username
              )
              .then(admin => {
                let lista = [];
                camine.data.forEach((camin) => {
                  admin.data.forEach((adminCamin, index) => {
                    if (camin.ID_Camin === adminCamin.ID_Camin) {
                      let caminNou = {
                        key: index,
                        text: camin.DenumireCamin,
                        value: camin.ID_Camin
                      };
                      lista.push(caminNou);
                    }
                  });
                });
                this.setState({listaCamine: lista});
                console.log(this.state.listaCamine);
                if (lista.length > 0) {
                  this.setState({caminSelectat: lista[0].value, idCaminSelectat: lista[0].value}) //caminSelectat/idCamin=numele/id camin aferent adminului din anul curent
                  localAxios
                    .get(
                      "DateIntoarcereCamin/CameraListByCaminAnUniv?iD_Camin=" +
                      lista[0].value +
                      "&iD_AnUniv=" +
                      anUniv.data.ID_AnUniv
                    )
                    .then(camere => {
                      this.setState({detaliiCamere: camere.data})
                      console.log(this.state.detaliiCamere)
                    })
                  axios
                    .get(
                      "api/Camin/CaminEtajListByCamin?iD_Camin=" +
                      lista[0].value +
                      "&iD_AnUniv=" +
                      anUniv.data.ID_AnUniv
                    )
                    .then(etaje => {
                      let temp = [];
                      etaje.data.forEach((et, index) => {
                        let etaj = {
                          key: index,
                          value: et.Etaj,
                          text: "Etajul " + et.Etaj
                        };
                        temp.push(etaj);
                      });
                      this.setState({listaEtaje: temp});
                      console.log(this.state.listaEtaje);
                    });
                }
              })
          });
      });
  }

  handleCheckbox = (event) => {
    {
      this.state.checked == true ? this.setState({checked: false}) : this.setState({checked: true})
    }
    console.log(this.state.checked)
    const value = event.target.value;
    const check = event.target.checked
    console.log(check)
    if ((check === true) && (this.state.idCameraChecked.includes(value) === false)) {
      let newList = [...this.state.idCameraChecked];
      newList.push(value);
      this.setState({idCameraChecked: newList});
    }

    console.log(value)
    console.log(this.state.idCameraChecked.includes(value))
    if ((check == false) && (this.state.idCameraChecked.includes(value) === true)) {
      for (let i = 0; i < this.state.idCameraChecked.length; i++)
        if (this.state.idCameraChecked[i] === value) {
          let newList = [...this.state.idCameraChecked];
          newList.splice(i, 1);
          this.setState({idCameraChecked: newList})
        }
    }
    console.log("Array din handle: " + this.state.idCameraChecked)
  };

  alegeEtajul = (e, etajSelectat) => { //filtru pentru afisare
    this.setState(etajSelectat)
    const etajSelectat1 = etajSelectat.value
    this.setState({etajCurent: etajSelectat1})
    console.log(this.state.etajCurent);
  }

  selectieCamin = (e, selectieNoua) => {
    console.log(selectieNoua)
    const camin = e.target.text
    this.setState({detaliiCamere: []});
    this.setState({listaEtaje: []});
    this.setState({etajCurent: null});
    this.setState({caminSelectat: selectieNoua.value});
    // this.setState({etajSelectat: null});
    this.setState({loaded: false});

    localAxios
      .get(`DateIntoarcereCamin/CameraListByCaminAnUniv?iD_Camin=${selectieNoua.value}&ID_AnUniv=${this.state.anSelectat}`)
      .then(camere => {
        this.setState({detaliiCamere: camere.data})
        console.log(this.state.detaliiCamere)
      })
    axios
      .get(
        "api/Camin/CaminEtajListByCamin?iD_Camin=" +
        selectieNoua.value +
        "&iD_AnUniv=" +
        this.state.anSelectat
      )
      .then(etaje => {
        let temp = [];
        etaje.data.forEach((et, index) => {
          let etaj = {
            key: index,
            value: et.Etaj,
            label: "Etajul " + et.Etaj
          };
          temp.push(etaj);
        });
        this.setState({listaEtaje: temp});
        console.log(this.state.listaEtaje);
      });

  };
  returneazaDenumireCamin = idCamin => {
    let nume = null;
    this.state.listaCamine.forEach((item) => {
      if (item.value === idCamin) nume = item.text;
    })
    return nume;
  }

  salveazaDate = () => {
    console.log(this.state.startDate._d);
    console.log(this.state.endDate);
    for (let i = 0; i < this.state.idCameraChecked.length; i++) {
      let dateCamere = {
        ID_Camera: this.state.idCameraChecked[i],
        DataInceput: moment(this.state.startDate).format("YYYY-MM-DD"),
        DataSfarsit: moment(this.state.endDate).format("YYYY-MM-DD")
      }
      console.log(this.state.startDate, this.state.endDate)

      let cameraList = []
      console.log('id camera', this.state.idCameraChecked)
      let dataInceput = this.state.startDate.format("YYYY-MM-DD")
      let dataSfarsit = this.state.endDate.format("YYYY-MM-DD")
      for (let id of this.state.idCameraChecked) {
        cameraList.push({
          ID_Camera: id,
          DataInceput: dataInceput,
          DataSfarsit: dataSfarsit
        })
      }
      console.log("camera list", cameraList)

      localAxios
        .post("DateIntoarcereCamin/AdaugareDataIntoarcereCameraCaminList", cameraList)
        .then(response => {
          console.log(response);
          localAxios
            .get(`DateIntoarcereCamin/CameraListByCaminAnUniv?iD_Camin=${this.state.caminSelectat}&ID_AnUniv=${this.state.anSelectat}`)
            .then(camere => {
              this.setState({detaliiCamere: camere.data})
              console.log(this.state.detaliiCamere)
            })

          var checkboxes = document.querySelectorAll('input[type=checkbox]')
          for (var j in checkboxes) {
            if (checkboxes[j].checked == true) {
              console.log(checkboxes[j].value)
              checkboxes[j].checked = false;
            }
          }
          this.setState({idCameraChecked: []})
          console.log(this.state.idCameraChecked)
        })
        .catch(error => {
          alert("a aparut o eroare");
        })
    }
  }



  render() {
    return (
      <div>
        <h1 style={{textAlign: 'center'}}>
          Pagină Administrator {this.returneazaDenumireCamin(this.state.caminSelectat)}{" "}</h1>
        <Menu widths={3} className="menu2">
          <Menu.Item>
            <Dropdown
              className="dropdown1"
              fluid
              selection
              search
              placeholder='Selectați caminul'
              options={this.state.listaCamine}
              onChange={this.selectieCamin}
              value={this.state.caminSelectat}
            />
          </Menu.Item>
          <Menu.Item>
            <Dropdown
              className="dropdown2"
              fluid
              selection
              search
              placeholder='Alegeți etajul'
              onChange={this.alegeEtajul}
              options={this.state.listaEtaje}
              value={this.state.etajCurent}/>
          </Menu.Item>
          <Menu.Item className="button3"><Button type="button" primary>Descarcă Rapoarte</Button></Menu.Item>
        </Menu>
        <div>
          <Segment
            textAlign={"center"}
            style={{marginLeft: 'auto', marginRight: 'auto'}}>
            <div style={{marginLeft: "auto", marginRight: "auto"}}>
              <span>
                <DateRangePicker
                  startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                  startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
                  endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                  endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
                  onDatesChange={({startDate, endDate}) => this.setState({
                    startDate,
                    endDate
                  })} // PropTypes.func.isRequired,
                  focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                  onFocusChange={focusedInput => this.setState({focusedInput})} // PropTypes.func.isRequired,
                />
              </span>
              <span>
              <Button
                disabled={!this.state.startDate || !this.state.endDate}
                type="button"
                color='green'
                icon='check'
                className="saveButton"
                onClick={this.salveazaDate}
                content='Salvează Modficările'
              />
            </span>
            </div>
            <div>
              <Grid>
                <Grid columns={3}>
                  {(this.state.detaliiCamere
                    .filter((camere, index) => !this.state.etajCurent ? true : camere.Etaj === this.state.etajCurent)
                    .map((show, index) => {
                        return (
                          <Grid.Column width={5} key={show.ID_Camera}>
                            <Camera
                              key={show.ID_Camera}
                              value={show.ID_Camera}
                              name={show.NumarCamera}
                              handleCheckbox={this.handleCheckbox}
                              ID_Camera={show.ID_Camera}
                              NumarCamera={show.NumarCamera}
                              idCamin={this.state.idCamin}
                              verificat={this.state.verificat}
                              dataInceput={show.DataInceputIntoarceri}
                              dataSfarsit={show.DataSfarsitIntoarceri}
                            />
                          </Grid.Column>
                        )
                      }
                    ))}
                </Grid></Grid></div>
          </Segment>
        </div>
      </div>

    )

  }
}

export default AlocareTermenCamera;
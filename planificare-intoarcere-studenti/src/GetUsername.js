/**
 *  Metoda care returneaza numele utilizatorului care este logat pe Intranet
 */
export const getUsername = () => {
    if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
        //pe aceasta ramura e codul care se executa in mediul de development. E hardcodat sa returneze un mail de student
        return "";
    } else {
        //ramura pentru mediul din productie care ia valoarea din tag-ul de html care extrage username-ul din DNN. Tag-ul esta e in index.html
        let element = document.getElementById("userid");
        return element.value;
    }
};

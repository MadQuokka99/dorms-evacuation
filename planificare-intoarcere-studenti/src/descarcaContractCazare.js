import axios from "../src/APIs";
/**
 * Functie care permite descarcarea contractului de cazare al unui student
 * @method
 * @param ID_Cazare
 * @param numeStudent - numele care va aparea in numele documentului care va fi descarcat
 */
export const descarcaContractCazare = (ID_Cazare, numeStudent = null) => {
    return axios
        .get(
            `Cazare/CazareGetPDF?ID_CererePrecazare=-1&ID_Cazare=${ID_Cazare}&tipDocxPdf=pdf`,
            {
                headers: { "Content-Type": "application/json" },
                responseType: "blob",
            }
        )
        .then((response) => {
            //se ia numele raportului din headerele trimise de pe server
            let filename = "";
            let disposition = response.headers["content-disposition"];
            if (disposition && disposition.indexOf("attachment") !== -1) {
                let filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                let matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) {
                    filename = matches[1].replace(/['"]/g, "");
                }
            }
            if (!filename) {
                filename = "Raport.xls";
            }
            //se creeaza un element si simuleaza un click pe ele pentru a face descarcarea fisierului in browser
            const downloadUrl = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement("a");
            link.href = downloadUrl;
            link.setAttribute("download", filename); //any other extension
            document.body.appendChild(link);
            link.click();
            link.remove();
        });
};